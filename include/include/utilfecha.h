
#define MINUTO 60
#define HORA 3600
#define ANIO 360
#define MES 30
#define NRO_MESES 12

void parseSegToHoras(int seg);
void parseDiasToAnios(int days);
char* getMonth(int n);
void parseToFecha(char* fecha);


